package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "Unlock a user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        if (serviceLocator == null) throw new EmptyObjectException();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
