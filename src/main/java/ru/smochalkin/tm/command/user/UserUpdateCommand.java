package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-update";
    }

    @Override
    @NotNull
    public String description() {
        return "Update a user.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter first name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateById(userId, firstName, lastName, middleName);
    }

    @Override
    @NotNull
    public Role[] roles() {
        return Role.values();
    }

}
